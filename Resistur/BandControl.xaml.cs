﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Windows.UI;
using System.ComponentModel;
using System.Runtime.CompilerServices;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Resistur
{
    public sealed partial class MyUserControl1 : UserControl, INotifyPropertyChanged
    {
        private const int MAX_BANDS = 9;
        private const int MIN_BANDS = 0;
        private float resistance = 0;
        private int band1Counter = 0;
        private int band2Counter = 0;
        private int band3Counter = 0;
        
        public float Resistance
        {
            get { return resistance; }
            set 
            { 
                changeBands(value);
            }
        }
        
        public MyUserControl1()
        {
            this.InitializeComponent();
            band1.Fill = setBandColor(0);
            band2.Fill = setBandColor(0);
            band3.Fill = setBandColor(0);
        }

        // Up/Down Button Changes
        private void upBand1_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            
        }

        // Helper Functions
        private SolidColorBrush setBandColor(int counter)
        {
            SolidColorBrush scb = new SolidColorBrush();
            switch (counter)
            {
                case 0:
                    scb.Color = Color.FromArgb(255, 0, 0, 0);
                    break;
                case 1:
                    scb.Color = Color.FromArgb(255, 125, 74, 2);
                    break;
                case 2:
                    scb.Color = Color.FromArgb(255, 255, 0, 0);
                    break;
                case 3:
                    scb.Color = Color.FromArgb(255, 255, 115, 0);
                    break;
                case 4:
                    scb.Color = Color.FromArgb(255, 255, 234, 0);
                    break;
                case 5:
                    scb.Color = Color.FromArgb(255, 0, 255, 0);
                    break;
                case 6:
                    scb.Color = Color.FromArgb(255, 0, 0, 255);
                    break;
                case 7:
                    scb.Color = Color.FromArgb(255, 155, 0, 255);
                    break;
                case 8:
                    scb.Color = Color.FromArgb(255, 166, 166, 166);
                    break;
                case 9:
                    scb.Color = Color.FromArgb(255, 255, 255, 255);
                    break;
                case 10:
                    scb.Color = Color.FromArgb(255, 214, 207, 0);
                    break;
            }
            return scb;
        }

        private void calcResistance(int b1, int b2, int b3)
        {
            if (b3 == 10 || b1 == 0)
            {
                resistance = b1 + ((float)b2 / 10);
            }
            else
            {
                if (b2 == 0)
                {
                    resistance = (combine(b1, b2) * 10) * (Int64)Math.Pow(10, b3);
                }
                else
                {
                    resistance = combine(b1, b2) * (Int64)Math.Pow(10, b3);
                }
            }
            NotifyPropertyChanged("Resistance");            
        }

        private void changeBands(float resistance)
        {
            int first;
            int second ;
            int numOfZeros;
            string stRep = resistance.ToString();

            first = Int32.Parse(stRep.Substring(0, 1)); // this will always be true

            if (stRep.Length > 1)
            {
                // Check length and then for E in position 1 - Ex. 7E+07
                if (stRep.Substring(1, 1).CompareTo("E") == 0)
                {
                    second = 0;
                    //numOfZeros = 9;
                    numOfZeros = Int32.Parse(stRep.Substring(3)) - 1;
                }
                // Check length and then for E in position 3 - Ex. 7.1E+07
                else if (stRep.Length > 6 && stRep.Substring(3, 1).CompareTo("E") == 0)
                {
                    second = Int32.Parse(stRep.Substring(2, 1));
                    numOfZeros = Int32.Parse(stRep.Substring(5)) - 1;
                }
                // Ex. 7.1
                else if (stRep.Length == 3 && stRep.Substring(1,1).CompareTo(".") == 0)
                {
                    second = Int32.Parse(stRep.Substring(2, 1));
                    numOfZeros = 10;
                }
                else
                {
                    // SHOULD NOT NEED THIS WTF?
                    if (stRep.Substring(1, 1).CompareTo(".") == 0)
                    {
                        second = Int32.Parse(stRep.Substring(2, 1));
                    }
                    else
                    {
                        second = Int32.Parse(stRep.Substring(1, 1));
                    }

                    if (second == 0)
                    {
                        numOfZeros = stRep.Split('0').Length - 2;
                    }
                    else
                    {
                        numOfZeros = stRep.Split('0').Length - 1;
                    }
                }
            }
            else // Ex. 7
            {
                second = 0;
                numOfZeros = 10;
            }

            band1Counter = first;
            band2Counter = second;
            band3Counter = numOfZeros;

            bandHelper(band1Counter, band2Counter, band3Counter);
        }

        // Event Handlers to Propagate Resistance Changes in Data Binding
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private int combine(int a, int b)
        {
            int times = 1;
            while (times <= b)
                times *= 10;
            return a * times + b;
        } 

        // Band Codes Up Buttons
        private void upBand1_Tapped(object sender, TappedRoutedEventArgs e)
        {
            band1Counter++;

            if (band1Counter > MAX_BANDS)
            {
                band1Counter = MIN_BANDS;
            }
            calcResistance(band1Counter, band2Counter, band3Counter);
            bandHelper(band1Counter, band2Counter, band3Counter);
        }

        private void upBand2_Tapped(object sender, TappedRoutedEventArgs e)
        {
            band2Counter++;

            if (band2Counter > MAX_BANDS)
            {
                band2Counter = MIN_BANDS;
            }
            calcResistance(band1Counter, band2Counter, band3Counter);
            bandHelper(band1Counter, band2Counter, band3Counter);
        }

        private void upBand3_Tapped(object sender, TappedRoutedEventArgs e)
        {
            band3Counter++;
            if (band3Counter > MAX_BANDS + 1)
            {
                band3Counter = MIN_BANDS;
            }
            calcResistance(band1Counter, band2Counter, band3Counter); // problem is here!?!?!
            //changeBands(resistance);
            bandHelper(band1Counter, band2Counter, band3Counter);
        }
        
        // Band Codes Down Button
        private void downBand1_Tapped(object sender, TappedRoutedEventArgs e)
        {
            band1Counter--;
            if (band1Counter < MIN_BANDS)
            {
                band1Counter = MAX_BANDS;
            }
            calcResistance(band1Counter, band2Counter, band3Counter);
            bandHelper(band1Counter, band2Counter, band3Counter);
        }

        private void downBand2_Tapped(object sender, TappedRoutedEventArgs e)
        {
            band2Counter--;

            if (band2Counter < MIN_BANDS)
            {
                band2Counter = MAX_BANDS;
            } 
            calcResistance(band1Counter, band2Counter, band3Counter);
            bandHelper(band1Counter, band2Counter, band3Counter);
        }

        private void downBand3_Tapped(object sender, TappedRoutedEventArgs e)
        {
            band3Counter--;

            if (band3Counter < MIN_BANDS)
            {
                band3Counter = 10;
            }

            calcResistance(band1Counter, band2Counter, band3Counter);
            bandHelper(band1Counter, band2Counter, band3Counter);
        }

        private void bandHelper(int b1, int b2, int b3)
        {
            band1.Fill = setBandColor(b1);
            band2.Fill = setBandColor(b2);
            band3.Fill = setBandColor(b3);
        }
    }
}

