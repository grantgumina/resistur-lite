﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Windows.UI.ApplicationSettings;
using Microsoft.Advertising.WinRT.UI;
using System.Text.RegularExpressions;

namespace Resistur
{
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            SettingsPane.GetForCurrentView().CommandsRequested += ShowPrivacyPolicy;
        }

        // PRIVACY POLICY
        // Method to add the privacy policy to the settings charm
        private void ShowPrivacyPolicy(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            SettingsCommand privacyPolicyCommand = new SettingsCommand("privacyPolicy", "Privacy Policy", (uiCommand) => { LaunchPrivacyPolicyUrl(); });
            args.Request.ApplicationCommands.Add(privacyPolicyCommand);
        }

        // Method to launch the url of the privacy policy
        async void LaunchPrivacyPolicyUrl()
        {
            Uri privacyPolicyUrl = new Uri("http://grantgumina.com/resistur/privacypolicy.html");
            var result = await Windows.System.Launcher.LaunchUriAsync(privacyPolicyUrl);
        }

        // AD ERROR HANDLING
        private void OnAdError(object sender, AdErrorEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("AdControl error (" + ((AdControl)sender).Name + "): " + e.Error + " ErrorCode: " + e.ErrorCode.ToString());
        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            errorTxt.Text = "";

            // Only allow numbers, '.', ',', 'e', and 'E'
            Regex r = new Regex("^[eE0-9.,+]*$");
            if (!r.IsMatch(resistanceTxt.Text))
            {
                errorTxt.Text = "Error. Please enter a valid number.";
            }
        }
    }
}
